package junia.lab03.core.dao;

import junia.lab03.core.entity.GenericEntity;
import junia.lab03.core.repository.GenericRepository;
import junia.lab03.domain.dao.GenericDAO;
import org.dozer.DozerBeanMapper;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

//TODO create 4 DAOs which inherits this abstract class
public abstract class AbstractDAO<T, U extends GenericEntity> implements GenericDAO<T> {

    private final GenericRepository<U> genericRepository;

    private final DozerBeanMapper mapper;

    private final Class<T> dataClazz;
    private final Class<U> entityClazz;

    protected AbstractDAO(GenericRepository<U> genericRepository) {
        this.genericRepository = genericRepository;
        this.mapper = new DozerBeanMapper();
        Type genericSuperclass = getClass().getGenericSuperclass();
        this.dataClazz = (Class<T>) ((ParameterizedType)genericSuperclass).getActualTypeArguments()[0];
        this.entityClazz = (Class<U>) ((ParameterizedType)genericSuperclass).getActualTypeArguments()[1];

    }


    @Override
    public void deleteAll() {
        this.genericRepository.deleteAll();
    }

    @Override
    public void save(T objectToSave) {
        this.genericRepository.save(mapper.map(objectToSave, entityClazz));

    }

    @Override
    public long count() {
        return this.genericRepository.count();
    }

    @Override
    public List<T> findAll() {
        return this.genericRepository.findAll().stream().map(u -> mapper.map(u, dataClazz)).collect(Collectors.toList());
    }

    public GenericRepository<U> getGenericRepository() {
        return genericRepository;
    }
}
